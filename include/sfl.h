// Skinetic Flash Loader
// Copyright (C) 2022 Actronika SAS
//     Author: Sylvain Gaultier <sylvain.gaultier@actronika.com>

#ifndef SKINETIC_FLASH_LOADER_LIBRARY_H
#define SKINETIC_FLASH_LOADER_LIBRARY_H

#ifdef __cplusplus
#include <cstddef>
#include <cstdint>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#ifdef _WIN32
#   ifdef SFL_EXPORT_API
#       define SFL_API __declspec(dllexport)
#   else
#       define SFL_API __declspec(dllimport)
#   endif
#else
#   ifdef SFL_EXPORT_API
#       define SFL_API __attribute__((visibility("default")))
#   else
#       define SFL_API
#   endif
#endif

#ifdef __cplusplus
#   define SFL_C_API extern "C" SFL_API
#else
#   define SFL_C_API SFL_API
#endif

/** Opaque pointer used as a handle for SFL instances */
typedef void* sfl_handle;

/**
 * Error codes
 */
typedef enum {
    /** Operation completed successfully */
    SFL_SUCCESS = 0,
    /** Failed to dynamically allocate memory */
    SFL_ERR_MEMORY = -1,
    /** One of the provided parameters was incorrect */
    SFL_ERR_INVALID_PARAM = -2,
    /** The USB HID connection with Skinetic failed */
    SFL_ERR_CONNECTION = -3,
    /** The HID communication with Skinetic failed */
    SFL_ERR_COMMUNICATION = -4,
    /** Internal error in the Skinetic bootloader */
    SFL_ERR_INTERNAL = -5,
    /** The Skinetic bootloader failed to answer before the timeout */
    SFL_ERR_TIMEOUT = -6,
    /** Initialization-related error (missing init, impossible init, ...) */
    SFL_ERR_INIT = -7,
    /** Other error */
    SFL_ERR_OTHER = -127,
} sfl_err_t;

/** Log levels definition */
typedef enum {
    SFL_LOG_TRACE = 0,
    SFL_LOG_DEBUG = 1,
    SFL_LOG_INFO = 2,
    SFL_LOG_WARNING = 3,
    SFL_LOG_ERROR = 4,
    SFL_LOG_FATAL = 5,
} sfl_log_level_t;

/** Signature of the callback function that can be registered to receive logs */
typedef void (*sfl_log_callback_t)(sfl_log_level_t level, const char* source,
        const char* message);

/* **************************************** *
 * Get build information about this library *
 * **************************************** */

/**
 * @return major (version is major.minor.micro)
 */
SFL_C_API unsigned int sfl_version_major();

/**
 * @return minor (version is major.minor.micro)
 */
SFL_C_API unsigned int sfl_version_minor();

/**
 * @return micro (version is major.minor.micro)
 */
SFL_C_API unsigned int sfl_version_micro();

/**
 * @return the version as a string [major].[minor].[micro]
 */
SFL_C_API const char* sfl_version_string();

/**
 * Same as `sfl_version_string` but with extra information.
 * The version string has the following format:
 * [major].[minor].[micro]-[extra commits]-[commit hash]-[dirty]-[broken]
 *
 * @return the detailed version string.
 */
SFL_C_API const char* sfl_version_details();

/* ***************** *
 * Configure logging *
 * ***************** */

/**
 * Set the logging level, only log messages of level greater or equal to the
 * provided `level` will call the logging callback.
 *
 * @param level the logging level.
 */
SFL_C_API void sfl_log_set_level(sfl_log_level_t level);

/**
 * Set the logging callback function. This callback will be called every time a
 * log message is produced by the library.
 * The callback function takes the following arguments:
 *   sfl_log_level_t level: the level of the log message
 *   const char* source: string giving information about the line of code that
 *                       send the log
 *   const char* message: string containing the log message
 * Set the callback to NULL to disable logging.
 *
 * @param callback the callback function.
 */
SFL_C_API void sfl_log_set_callback(sfl_log_callback_t callback);

/* ****************************************** *
 * Initialization functions and error getters *
 * ****************************************** */

/**
 * Initialize the underlying HID library.
 * This function must be called once before attempting to call `sfl_auto_open`,
 * `sfl_open_by_index` or `sfl_open_by_path`.
 *
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_init();

/**
 * De-initialize the underlying HID library.
 * This function can be called once after closing every previously opened
 * `sfl_handle`.
 *
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_deinit();

/**
 * @return the error code that was produced by the last call to a function of
 *         this library. This is especially useful for the few functions that
 *         don't return an error code.
 */
SFL_C_API sfl_err_t sfl_last_error();

/**
 * @return the `sfl_last_error` as a human-readable string.
 */
SFL_C_API const char* sfl_last_error_as_string();

/* ********************** *
 * Scan available devices *
 * ********************** */

/**
 * Scan the available HID devices and update the internal device list
 * accordingly. Only Skinetic bootloader devices will be listed.
 *
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_update_device_list();

/**
 * @return the number of entries in the internal device list.
 */
SFL_C_API size_t sfl_device_list_size();

/**
 * Get the serial number of a USB device from the internal device list.
 *
 * @param device_index index in the internal device list.
 * @return the serial number of the USB device.
 */
SFL_C_API const wchar_t* sfl_get_device_serial_number(size_t device_index);

/**
 * Get the manufacturer string of a USB device from the internal device list.
 *
 * @param device_index index in the internal device list.
 * @return the manufacturer string of the USB device.
 */
SFL_C_API const wchar_t* sfl_get_device_manufacturer_string(size_t device_index);

/**
 * Get the product string of a USB device from the internal device list.
 *
 * @param device_index index in the internal device list.
 * @return the product string of the USB device.
 */
SFL_C_API const wchar_t* sfl_get_device_product_string(size_t device_index);

/* ****************** *
 * Open/Close devices *
 * ****************** */

/**
 * Open the first Skinetic bootloader device found. The internal device list
 * will be automatically updated when calling this function.
 *
 * @return the handle in case of success, NULL in case of error.
 */
SFL_C_API sfl_handle sfl_auto_open();

/**
 * Open the device at the given `device_index` in the internal device list. The
 * internal device list must be up to date before calling this function.
 *
 * @param device_index the index in the internal device list.
 * @return the handle in case of success, NULL in case of error.
 */
SFL_C_API sfl_handle sfl_open_by_index(size_t device_index);

/**
 * Close the device.
 *
 * @param handle the sfl handle.
 */
SFL_C_API void sfl_close(sfl_handle handle);

/**
 * Reset the Skinetic bootloader. This will cause a power-off of the device so
 * we also close the device.
 * On success, the handle is released and can be discarded. On failure the
 * handle remains valid.
 *
 * @param handle the sfl handle.
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_reset_and_close(sfl_handle handle);

/* ********************** *
 * Flash memory functions *
 * ********************** */

/**
 * Erase a given flash memory area.
 * The start address must be aligned on 0x40000 (the size of a flash memory
 * region). Only a full region can be erased, so more bytes than expected will
 * be erased if the length is not aligned on 0x40000.
 *
 * @param handle the sfl handle.
 * @param start_address address of the first byte to erase.
 * @param length number of bytes to erase (more may be erased if not aligned).
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_flash_erase(sfl_handle handle,
        uint32_t start_address, uint32_t length);

/**
 * Read the flash memory.
 * (It is more efficient to align the start address with 0x200).
 *
 * @param handle the sfl handle.
 * @param start_address address of the first byte to read.
 * @param length number of bytes to read.
 * @param data buffer to put the read data into.
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_flash_read(sfl_handle handle,
        uint32_t start_address, uint32_t length, uint8_t* data);

/**
 * Write the flash memory.
 * (It is more efficient to align the start address with 0x200).
 *
 * @param handle the sfl handle.
 * @param start_address address of the first byte to write.
 * @param length number of bytes to write.
 * @param data buffer containing the data to write.
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_flash_write(sfl_handle handle,
        uint32_t start_address, uint32_t length, const uint8_t* data);

/**
 * Get the Skinetic bootloader version string.
 * The function outputs a `const char*` that will be valid until the device is
 * closed.
 *
 * @param handle the sfl handle.
 * @param[out] version the bootloader version string (null-terminated).
 * @return the error code.
 */
SFL_C_API sfl_err_t sfl_bootloader_version(sfl_handle handle, const char** version);

#endif // SKINETIC_FLASH_LOADER_LIBRARY_H

# Flashloader



## Getting started

Download or clone the repository on your computer. If you have downloaded the project, unzip the file.

## Install requirements

Go into the Flashloader folder and type this command:

```
pip3 install -r requirements.txt
```

## Start application

Go into the "bin" folder of the Flashloader installation folder and type this command:

```
Python3 skinetic-flash-loader-gui.py
```
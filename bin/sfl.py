#  Skinetic Flash Loader
#  Copyright (C) 2022 Actronika SAS
#      Author: Sylvain Gaultier <sylvain.gaultier@actronika.com>

# This is a python3 interface for a C shared library.
# The library binary suited for your platform must be located in the same folder
# as this script, with the following name:
# Windows: sfl.dll
# Linux: libsfl.so
# Darwin: libsfl.dylib

from enum import Enum, IntEnum
from ctypes import *
from os.path import dirname, abspath, join
import typing
import platform


class _SFLErrorCode(IntEnum):
    SUCCESS = 0
    ERR_MEMORY = -1
    ERR_INVALID_PARAM = -2
    ERR_CONNECTION = -3
    ERR_COMMUNICATION = -4
    ERR_INTERNAL = -5
    ERR_TIMEOUT = -6
    ERR_INIT = -7
    ERR_OTHER = -127


def _handle_error_code(code: int) -> int:
    if code == _SFLErrorCode.SUCCESS:
        return code
    elif code == _SFLErrorCode.ERR_MEMORY:
        raise MemoryError("SFL_ERR_MEMORY")
    elif code == _SFLErrorCode.ERR_INVALID_PARAM:
        raise ValueError("SFL_ERR_INVALID_PARAM")
    elif code == _SFLErrorCode.ERR_CONNECTION:
        raise OSError("SFL_ERR_CONNECTION")
    elif code == _SFLErrorCode.ERR_COMMUNICATION:
        raise OSError("SFL_ERR_COMMUNICATION")
    elif code == _SFLErrorCode.ERR_INTERNAL:
        raise RuntimeError("SFL_ERR_INTERNAL")
    elif code == _SFLErrorCode.ERR_TIMEOUT:
        raise TimeoutError("SFL_ERR_TIMEOUT")
    elif code == _SFLErrorCode.ERR_INIT:
        raise RuntimeError("SFL_ERR_INIT")
    elif code == _SFLErrorCode.ERR_OTHER:
        raise RuntimeError("SFL_ERR_OTHER")
    else:
        raise RuntimeError("Unknown error code: " + str(code))


class SFL:
    """
    Skinetic Flash Loader
    """

    class LogLevel(Enum):
        """ Log levels definition """
        TRACE = 0
        DEBUG = 1
        INFO = 2
        WARNING = 3
        ERROR = 4
        FATAL = 5

        @classmethod
        def from_param(cls, obj):
            if not isinstance(obj, cls):
                raise TypeError
            return obj.value

    @staticmethod
    @CFUNCTYPE(None, c_int, c_char_p, c_char_p)
    def _log_callback(level: int, source: bytes, message: bytes):
        if SFL._USER_LOG_CALLBACK is not None:
            SFL._USER_LOG_CALLBACK(SFL.LogLevel(level), source.decode("ascii"),
                                   message.decode("ascii"))

    class Device:
        """ Structure to store information about a USB HID device """
        def __init__(self, serial_nb: str, manf: str, product: str):
            self.serial_number = serial_nb
            self.manufacturer = manf
            self.product = product

        def __str__(self):
            return ("USB device\n  S/N: " + self.serial_number +
                    "\n  Manufacturer: " + self.manufacturer + "\n  Product: " +
                    self.product + "\n")

    DEVICE_LIST: typing.List[Device] = []  # List of USB HID devices detected in
    # the last call to `update_device_list`

    _C_LIB = None
    _INSTANCE_COUNT = 0
    _USER_LOG_CALLBACK = None
    _SFL_C_API = {
        "sfl_version_major": (c_uint, []),
        "sfl_version_minor": (c_uint, []),
        "sfl_version_micro": (c_uint, []),
        "sfl_version_string": (c_char_p, []),
        "sfl_version_details": (c_char_p, []),
        "sfl_log_set_level": (None, [LogLevel]),
        "sfl_log_set_callback": (None, [CFUNCTYPE(None, c_int, c_char_p, c_char_p)]),
        "sfl_init": (_handle_error_code, []),
        "sfl_deinit": (_handle_error_code, []),
        "sfl_last_error": (_handle_error_code, []),
        "sfl_last_error_as_string": (c_char_p, []),
        "sfl_update_device_list": (_handle_error_code, []),
        "sfl_device_list_size": (c_size_t, []),
        "sfl_get_device_serial_number": (c_wchar_p, [c_size_t]),
        "sfl_get_device_manufacturer_string": (c_wchar_p, [c_size_t]),
        "sfl_get_device_product_string": (c_wchar_p, [c_size_t]),
        "sfl_auto_open": (c_void_p, []),
        "sfl_open_by_index": (c_void_p, [c_size_t]),
        "sfl_close": (None, [c_void_p]),
        "sfl_reset_and_close": (_handle_error_code, [c_void_p]),
        "sfl_flash_erase": (_handle_error_code, [c_void_p, c_uint32, c_uint32]),
        "sfl_flash_read": (_handle_error_code, [c_void_p, c_uint32, c_uint32, POINTER(c_uint8)]),
        "sfl_flash_write": (_handle_error_code, [c_void_p, c_uint32, c_uint32, POINTER(c_uint8)]),
        "sfl_bootloader_version": (_handle_error_code, [c_void_p, POINTER(c_char_p)]),
    }

    def __init__(self, device: typing.Union[None, int] = None):
        """
        Open a connection to the device.
        By default, the first compatible USB device found will be opened.
        If providing an integer argument, it will be interpreted as an index in
        the SFL.DEVICE_LIST list and the corresponding device will be opened.

        :param device: the device to connect to.
        """
        self._handle = None
        SFL._init_c_lib()
        if SFL._INSTANCE_COUNT == 0:
            SFL._static_call("sfl_init")
        SFL._INSTANCE_COUNT += 1
        if device is None:
            self._handle = SFL._static_call("sfl_auto_open")
        elif isinstance(device, int):
            self._handle = SFL._static_call("sfl_open_by_index", device)
        else:
            raise ValueError
        SFL._static_call("sfl_last_error")

    def __del__(self):
        """ Close the connection with the device """
        if SFL._C_LIB is not None:
            self._call("sfl_close")
            if SFL._INSTANCE_COUNT > 0:
                SFL._INSTANCE_COUNT -= 1
                if SFL._INSTANCE_COUNT == 0:
                    SFL._static_call("sfl_deinit")

    @staticmethod
    def version() -> typing.Tuple[int, int, int]:
        """
        :return: the library version as a tuple (major, minor, micro)
        """
        SFL._init_c_lib()
        major = SFL._static_call("sfl_version_major")
        minor = SFL._static_call("sfl_version_minor")
        micro = SFL._static_call("sfl_version_micro")
        return major, minor, micro

    @staticmethod
    def version_string() -> str:
        """
        :return: the version as a string [major].[minor].[micro]
        """
        SFL._init_c_lib()
        return SFL._static_call("sfl_version_string").decode('ascii')

    @staticmethod
    def version_details() -> str:
        """
        Same as `version_string` but with extra information.
        The version string has the following format:
        [major].[minor].[micro]-[extra commits]-[commit hash]-[dirty]-[broken]

        :return: the detailed version string.
        """
        SFL._init_c_lib()
        return SFL._static_call("sfl_version_details").decode('ascii')

    @staticmethod
    def log_set_level(level: LogLevel):
        """
        Set the logging level, only log messages of level greater or equal to
        the provided `level` will call the logging callback.

        :param level: the logging level.
        """
        SFL._init_c_lib()
        SFL._static_call("sfl_log_set_level", level)

    @staticmethod
    def log_set_callback(callback: typing.Optional[callable]):
        """
        Set the logging callback function. This callback will be called every
        time a log message is produced by the library.
        The callback function takes the following arguments:
          [SFL.LogLevel] level: the level of the log message
          [str] source: string giving information about the line of code that
                        send the log
          [str] message: string containing the log message
        Set the callback to None to disable logging.

        :param callback: the callback function.
        """
        SFL._USER_LOG_CALLBACK = callback

    @staticmethod
    def update_device_list():
        """
        Scan the available Skinetic devices and update SFL.DEVICE_LIST
        accordingly.
        """
        SFL._init_c_lib()
        if SFL._INSTANCE_COUNT == 0:
            SFL._static_call("sfl_init")
        try:
            SFL._static_call("sfl_update_device_list")
            SFL.DEVICE_LIST = []
            for i in range(SFL._static_call("sfl_device_list_size")):
                data = (SFL._static_call(f, i) for f in [
                    "sfl_get_device_serial_number",
                    "sfl_get_device_manufacturer_string",
                    "sfl_get_device_product_string",
                ])
                SFL.DEVICE_LIST.append(SFL.Device(*data))
        finally:
            if SFL._INSTANCE_COUNT == 0:
                SFL._static_call("sfl_deinit")

    def flash_erase(self, address: int, length: int):
        """
        Erase a given flash memory area.
        The address and the length must be aligned on 0x40000 (the size of a
        flash memory region).
        :param address: address of the first byte to erase.
        :param length: number of bytes to erase.
        """
        self._call("sfl_flash_erase", address, length)

    def flash_read(self, address: int, length: int) -> bytes:
        """
        Read the flash memory.
        (It is more efficient to align the address with 0x200).
        :param address: address of the first byte to read.
        :param length: number of bytes to read.
        :return: buffer containing the read data.
        """
        data = bytes(length)
        self._call("sfl_flash_read", address, length,
                   cast(data, POINTER(c_uint8)))
        return data

    def flash_write(self, address: int, data):
        """
        Write the flash memory.
        (It is more efficient to align the address with 0x200).
        :param address: address of the first byte to write.
        :param data: buffer containing the data to write.
        """
        self._call("sfl_flash_write", address, len(data),
                   cast(bytes(data), POINTER(c_uint8)))

    def bootloader_version(self) -> str:
        version = c_char_p()
        self._call("sfl_bootloader_version", byref(version))
        return version.value.decode('ascii')

    def reset_and_close(self):
        """
        Reset the board. On success this will close the connection. The SFL
        object is no longer valid and should be deleted.
        Otherwise, an exception is raised.
        """
        self._call("sfl_reset_and_close")
        self._handle = None

    @staticmethod
    def _init_c_lib():
        if SFL._C_LIB is not None:
            return
        root = abspath(dirname(__file__))
        s = platform.system()
        try:
            if s == "Windows":
                SFL._C_LIB = cdll.LoadLibrary(join(root, "sfl.dll"))
            elif s == "Linux":
                SFL._C_LIB = cdll.LoadLibrary(join(root, "libsfl.so"))
            elif s == "Darwin":
                SFL._C_LIB = cdll.LoadLibrary(join(root, "libsfl.dylib"))
            else:
                raise RuntimeError("Unsupported platform: " + s)
            SFL._init_c_lib_prototypes()
            SFL._static_call("sfl_log_set_callback", SFL._log_callback)
        except (Exception, KeyboardInterrupt):
            SFL._C_LIB = None
            raise

    @staticmethod
    def _init_c_lib_prototypes():
        for f_name, signature in SFL._SFL_C_API.items():
            c_func = getattr(SFL._C_LIB, f_name)
            c_func.restype = signature[0]
            c_func.argtypes = signature[1]

    @staticmethod
    def _static_call(f_name, *args):
        c_func = getattr(SFL._C_LIB, f_name)
        return c_func(*args)

    def _call(self, f_name, *args):
        c_func = getattr(SFL._C_LIB, f_name)
        return c_func(self._handle, *args)

#  Skinetic Flash Loader
#  Copyright (C) 2022 Actronika SAS
#      Author: Sylvain Gaultier <sylvain.gaultier@actronika.com>

import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QGroupBox,
                             QCheckBox, QComboBox, QLabel, QProgressBar,
                             QVBoxLayout, QHBoxLayout, QWidget, QMessageBox,
                             QListView, QFileDialog)
from PyQt5.QtGui import QCloseEvent, QPainter, QFontMetrics
from PyQt5.QtCore import Qt, pyqtSignal, QThread, QObject
from typing import Optional, Tuple, List, Any, Callable
from os.path import dirname
from bincopy import BinFile
from sfl import SFL

FLASH_START_ADDRESS = 0x60040000
FLASH_SIZE = 0x00FC0000
FLASH_REGION_SIZE = 0x40000
FLASH_PAGE_SIZE = 0x200

READ_STD_T = 5e-4
WRITE_STD_T = 1e-3
ERASE_STD_T = 0.500


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Skinetic Flash Loader")

        # Member variables
        self._skinetic: Optional[SFL] = None
        self._working_dir = "."
        self._hex: Optional[BinFile] = None
        self._task = TaskRunner(self)
        # noinspection PyUnresolvedReferences
        self._task.progressUpdate.connect(self._on_progress_update)

        # Widgets
        self._b_connect = QPushButton(self)
        self._b_connect.clicked.connect(self._on_connect)
        self._b_devices = ComboBox(self)
        # noinspection PyUnresolvedReferences
        self._b_devices.popupAboutToBeShown.connect(
            self._update_available_devices)
        self._l_bl_version = QLabel(self)
        self._l_bl_version.setTextInteractionFlags(Qt.TextSelectableByMouse)
        self._b_open = QPushButton("Open .hex file", self)
        self._b_open.clicked.connect(self._on_open)
        self._l_filename = ElidedLabel(self)
        self._c_verify = QCheckBox("Verify", self)
        self._c_verify.setChecked(True)
        self._c_verify.setToolTip("Read back the flash memory after writing")
        self._c_wipe = QCheckBox("Full erase", self)
        self._c_wipe.setToolTip("Erase all the flash memory before writing\n"
                                "(not only the necessary regions)")
        self._b_write = QPushButton("Write file", self)
        self._b_write.clicked.connect(self._on_write_flash)
        self._b_read = QPushButton("Read flash memory", self)
        self._b_read.clicked.connect(self._on_read_flash)
        self._b_wipe = QPushButton("Wipe flash memory", self)
        self._b_wipe.clicked.connect(self._on_wipe_flash)
        self._l_status = QLabel(self)
        self._l_status.setAlignment(Qt.AlignCenter)
        self._progress_bar = QProgressBar(self)
        self._progress_bar.setTextVisible(False)
        self._progress_bar.setRange(0, 100)
        self._b_cancel = QPushButton("Cancel", self)
        self._b_cancel.clicked.connect(self._on_cancel)

        # Layout
        w = QWidget(self)
        layout = QVBoxLayout(w)
        layout.addWidget(self._b_connect)
        h_layout = QHBoxLayout()
        h_layout.addWidget(QLabel("S/N:", self))
        h_layout.addWidget(self._b_devices, stretch=1)
        layout.addLayout(h_layout)
        h_layout = QHBoxLayout()
        h_layout.addWidget(QLabel("Bootloader version:", self))
        h_layout.addWidget(self._l_bl_version, stretch=1)
        layout.addLayout(h_layout)
        box = QGroupBox("Program flash memory", self)
        sub_layout = QVBoxLayout(box)
        h_layout = QHBoxLayout()
        h_layout.addWidget(self._b_open)
        h_layout.addWidget(self._l_filename, stretch=1)
        sub_layout.addLayout(h_layout)
        h_layout = QHBoxLayout()
        h_layout.addWidget(self._c_verify, stretch=1)
        h_layout.addWidget(self._c_wipe, stretch=1)
        sub_layout.addLayout(h_layout)
        h_layout = QHBoxLayout()
        h_layout.addStretch(1)
        h_layout.addWidget(self._b_write)
        sub_layout.addLayout(h_layout)
        layout.addWidget(box)
        h_layout = QHBoxLayout()
        h_layout.addWidget(self._b_read)
        h_layout.addWidget(self._b_wipe)
        layout.addLayout(h_layout)
        layout.addStretch(1)
        layout.addWidget(self._l_status)
        h_layout = QHBoxLayout()
        h_layout.addWidget(self._progress_bar, stretch=1)
        h_layout.addWidget(self._b_cancel)
        layout.addLayout(h_layout)
        h_layout = QHBoxLayout()
        l1 = QLabel("SFL C library version:", self)
        l1.setEnabled(False)
        l2 = QLabel(SFL.version_details(), self)
        l2.setEnabled(False)
        h_layout.addWidget(l1)
        h_layout.addWidget(l2, stretch=1)
        layout.addLayout(h_layout)
        self.setCentralWidget(w)

        # Init
        self._update_available_devices()
        self._update_widgets()

    def _update_widgets(self):
        if self._skinetic is None:
            self._b_connect.setText("Connect")
        else:
            self._b_connect.setText("Disconnect")
        self._b_devices.setEnabled(self._skinetic is None)
        self._b_write.setDisabled(self._skinetic is None or self._hex is None or
                                  self._is_task_running())
        self._b_read.setDisabled(self._skinetic is None or
                                 self._is_task_running())
        self._b_wipe.setDisabled(self._skinetic is None or
                                 self._is_task_running())
        self._b_cancel.setEnabled(self._is_task_running())
        self._b_connect.setDisabled(self._is_task_running())
        self._b_open.setDisabled(self._is_task_running())
        self._c_verify.setDisabled(self._is_task_running())
        self._c_wipe.setDisabled(self._is_task_running())

    def _update_available_devices(self):
        SFL.update_device_list()
        self._b_devices.clear()
        for d in SFL.DEVICE_LIST:
            if len(d.serial_number) == 0:
                self._b_devices.addItem("Unknown serial number")
            else:
                self._b_devices.addItem(d.serial_number)

    def _is_task_running(self) -> bool:
        return self._task.is_running()

    def _on_connect(self):
        if self._skinetic is not None:
            self._close_connection()
            return

        if self._b_devices.count() == 0:
            self._update_available_devices()
        if self._b_devices.count() == 0:
            self._close_connection(
                FileNotFoundError(), "No device connected to the computer")
            return

        try:
            self._skinetic = SFL(self._b_devices.currentIndex())
            self._l_bl_version.setText(self._skinetic.bootloader_version())
            self._update_widgets()
        except Exception as e:
            msg = ("Connection to device '" + self._b_devices.currentText() +
                   "' failed")
            self._update_available_devices()
            self._close_connection(e, msg)

    def _close_connection(self, error: Optional[Exception] = None,
                          msg: str = "Connection lost"):
        self._l_bl_version.setText("")
        if self._skinetic is not None:
            self._task.stop()
            self._skinetic = None
            self._update_available_devices()
        if error is not None and len(msg) > 0:
            QMessageBox.warning(self, "Connection error",
                                msg + "\n" + str(error))
        self._update_widgets()

    def _request_close(self) -> bool:
        if self._is_task_running():
            answer = QMessageBox.warning(
                self, "A task is still running",
                "Warning: a programming task is still running.\nDo you want to "
                "interrupt it ?", QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No)
            if answer == QMessageBox.Yes:
                self._close_connection()
                return True
            else:
                return False
        else:
            return True

    def closeEvent(self, event: QCloseEvent):
        if self._request_close():
            event.accept()
        else:
            event.ignore()

    def _on_open(self):
        path, _ = QFileDialog.getOpenFileName(
            self, "Open .hex file", self._working_dir,
            "Intel HEX files (*.hex)")
        if len(path) == 0:
            return
        self._l_filename.setText(path)
        self._l_filename.setToolTip(path)
        self._working_dir = dirname(path)
        task_list = [(BinFile, [path], 1)]
        self._start_task(task_list, self._on_open_end, "Parsing .hex file...", 50)

    def _on_open_end(self, returns: List[BinFile], error: Optional[Exception]):
        if error is None:
            self._hex = returns[0]
            if len(self._hex) == 0:
                error = ValueError("Empty .hex file.")
            if (self._hex.minimum_address < FLASH_START_ADDRESS or
                    self._hex.maximum_address > FLASH_START_ADDRESS + FLASH_SIZE):
                error = ValueError("This .hex file refers to memory addresses "
                                   "outside of the desired region.")
        if error is None:
            self._set_status(100, "File loaded successfully")
            self._update_widgets()
        else:
            self._hex = None
            self._l_filename.setText("")
            self._l_filename.setToolTip("")
            self._set_status(0, "Failed to parse .hex file")
            QMessageBox.critical(self, "Invalid HEX file",
                                 "Failed to load the file\n" + str(error))

    def _on_read_flash(self):
        task_list = [
            (self._skinetic.flash_read, [i, FLASH_PAGE_SIZE], READ_STD_T)
            for i in range(FLASH_START_ADDRESS,
                           FLASH_START_ADDRESS + FLASH_SIZE, FLASH_PAGE_SIZE)
        ]
        self._start_task(task_list, self._on_read_flash_end,
                         "Reading Flash memory...")

    def _on_read_flash_end(self, returns: List[bytes],
                           error: Optional[Exception]):
        if error is not None:
            msg = "Failed to read Flash memory"
            self._set_status(0, msg)
            QMessageBox.critical(self, "Error", msg + "\n" + str(error))
            if isinstance(error, OSError):
                self._close_connection()
            return
        self._set_status(100, "Flash memory read successfully")
        path, _ = QFileDialog.getSaveFileName(
            self, "Save as...", self._working_dir, "Intel HEX (*.hex)")
        if len(path) == 0:
            return
        task_list = [(self._save_hex_file, [path, returns], 1)]
        self._start_task(task_list, self._on_file_write_end,
                         "Writing .hex file...", 50)

    @staticmethod
    def _save_hex_file(path, data: List[bytes]):
        h = BinFile()
        offset = FLASH_START_ADDRESS
        for page in data:
            h.add_binary(page, offset)
            offset += len(page)
        with open(path, "w") as f:
            f.write(h.as_ihex())

    def _on_file_write_end(self, _, error: Optional[Exception]):
        if error is None:
            self._set_status(100, "File saved")
        else:
            msg = "Failed to save .hex file"
            self._set_status(0, msg)
            QMessageBox.critical(self, "Error", msg + "\n" + str(error))

    def _on_wipe_flash(self):
        task_list = [
            (self._skinetic.flash_erase, [i, FLASH_REGION_SIZE], ERASE_STD_T)
            for i in range(FLASH_START_ADDRESS,
                           FLASH_START_ADDRESS + FLASH_SIZE, FLASH_REGION_SIZE)
        ]
        self._start_task(task_list, self._on_wipe_flash_end,
                         "Erasing Flash memory...")

    def _on_wipe_flash_end(self, _, error: Optional[Exception]):
        if error is None:
            self._set_status(100, "Flash memory erased successfully")
        else:
            msg = "Failed to erase Flash memory"
            self._set_status(0, msg)
            QMessageBox.critical(self, "Error", msg + "\n" + str(error))
            if isinstance(error, OSError):
                self._close_connection()

    def _on_write_flash(self):
        if self._c_wipe.isChecked():
            start = FLASH_START_ADDRESS
            end = FLASH_START_ADDRESS + FLASH_SIZE
        else:
            start = self._hex.minimum_address - (self._hex.minimum_address % FLASH_REGION_SIZE)
            x = self._hex.maximum_address % FLASH_REGION_SIZE
            if x == 0:
                end = self._hex.maximum_address
            else:
                end = self._hex.maximum_address - x + FLASH_REGION_SIZE
        e_task_list = [
            (self._skinetic.flash_erase, [i, FLASH_REGION_SIZE], ERASE_STD_T)
            for i in range(start, end, FLASH_REGION_SIZE)
        ]
        w_task_list = [
            (self._skinetic.flash_write, [page.address, page.data], WRITE_STD_T)
            for page in self._hex.segments.chunks(FLASH_PAGE_SIZE)
        ]
        if self._c_verify.isChecked():
            r_task_list = [
                (self._skinetic.flash_read, [page.address, len(page.data)], READ_STD_T)
                for page in self._hex.segments.chunks(FLASH_PAGE_SIZE)
            ]
        else:
            r_task_list = []
        self._start_task(e_task_list + w_task_list + r_task_list,
                         self._on_write_flash_end, "Programming Flash memory...")

    def _on_write_flash_end(self, returns: List[Optional[bytes]],
                            error: Optional[Exception]):
        if error is None:
            verify_data = [data for data in returns if data is not None]
            if len(verify_data) > 0:
                for i, page in enumerate(self._hex.segments.chunks(FLASH_PAGE_SIZE)):
                    if verify_data[i] != page.data:
                        error = ValueError("Verify reported invalid data "
                                           "written to Flash memory")
                        break
        if error is None:
            self._set_status(100, "Flash memory successfully written")
        else:
            msg = "Failed to write Flash memory"
            self._set_status(0, msg)
            QMessageBox.critical(self, "Error", msg + "\n" + str(error))
            if isinstance(error, OSError):
                self._close_connection()

    def _on_cancel(self):
        answer = QMessageBox.warning(
            self, "Cancel current task ?",
            "You are about to interrupt a Flash memory operation.\nThis may "
            "leave the board in an unstable state.\nAre you sure ?",
            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if answer == QMessageBox.Yes:
            self._task.stop()

    def _on_progress_update(self, progress: int):
        self._progress_bar.setValue(progress)

    def _set_status(self, progress: int, message: str = ""):
        self._progress_bar.setValue(progress)
        self._l_status.setText(message)

    def _start_task(self, task_list, end_callback, status_msg="", progress=0):
        def end_cb(*args):
            self._update_widgets()
            end_callback(*args)
        self._task.set_task_list(task_list, end_cb)
        self._set_status(progress, status_msg)
        self._task.start()
        self._update_widgets()


class ComboBox(QComboBox):
    popupAboutToBeShown = pyqtSignal()

    def __init__(self, *args):
        super().__init__(*args)
        self.view().setResizeMode(QListView.Adjust)

    def showPopup(self):
        # noinspection PyUnresolvedReferences
        self.popupAboutToBeShown.emit()
        super().showPopup()


class ElidedLabel(QLabel):
    def paintEvent(self, event):
        painter = QPainter(self)
        metrics = QFontMetrics(self.font())
        elided = metrics.elidedText(self.text(), Qt.ElideLeft, self.width())
        painter.drawText(self.rect(), self.alignment(), elided)


class TaskRunner(QObject):
    progressUpdate = pyqtSignal(int)  # % of progress
    TaskList = List[Tuple[callable, list, float]]
    EndCallback = Callable[[List[Any], Optional[Exception]], None]

    class Pipe:
        def __init__(self, index: int = -1):
            self.index = index

    def __init__(self, parent):
        super().__init__(parent)
        self._thread = QThread(self)
        self._thread.run = self._run
        self._thread.finished.connect(self._on_thread_end)
        self._task_list: TaskRunner.TaskList = []
        self._end_callback: Optional[TaskRunner.EndCallback] = None
        self._returns = []
        self._error: Optional[Exception] = None
        self._interrupt = False
        self._current_progress = 0
        self._total_progress = 0
        self._last_progress_update = 0

    def _init_progress(self):
        self._total_progress = 0
        for _, __, t in self._task_list:
            self._total_progress += t
        self._current_progress = 0
        self._last_progress_update = 0

    def _update_progress(self):
        try:
            ratio = self._current_progress / self._total_progress
        except ZeroDivisionError:
            ratio = 0
        p = round(ratio * 100)
        if p != self._last_progress_update:
            self._last_progress_update = p
            # noinspection PyUnresolvedReferences
            self.progressUpdate.emit(p)

    def set_task_list(self, task_list: TaskList, end_callback: EndCallback):
        self._task_list = task_list
        self._end_callback = end_callback

    def start(self):
        self._returns = []
        self._error = None
        self._interrupt = False
        self._init_progress()
        self._thread.start()

    def stop(self):
        if self.is_running():
            self._interrupt = True
            if not self._thread.wait(5000):
                self._error = RuntimeError("Canceled by user")
                self._thread.terminate()

    def is_running(self) -> bool:
        return self._thread.isRunning()

    def _run(self):
        for f, args, t in self._task_list:
            try:
                self._process_args(args)
                result = f(*args)
            except Exception as e:
                self._error = e
                return
            self._returns.append(result)
            self._current_progress += t
            self._update_progress()
            if self._interrupt:
                self._error = RuntimeError("Canceled by user")
                return

    def _process_args(self, args: List):
        for i, arg in enumerate(args):
            if isinstance(arg, TaskRunner.Pipe):
                args[i] = self._returns[arg.index]

    def _on_thread_end(self):
        self._end_callback(self._returns, self._error)


def log_callback(level: SFL.LogLevel, source: str, message: str):
    print(level.name, message, source)


def except_hook(t, value, traceback):
    sys.__excepthook__(t, value, traceback)


if __name__ == '__main__':
    SFL.log_set_callback(log_callback)
    SFL.log_set_level(SFL.LogLevel.WARNING)
    sys.excepthook = except_hook
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    win.setMinimumSize(250, 300)
    win.setMaximumSize(500, 400)
    win.resize(300, 300)
    ret = app.exec()
    sys.exit(ret)
